<?php


/**
 * Menu callback.  Displays RSS feed with the latest entries of a user's blog.
 *
 * This function is based on blog.module's blog_feed_user().
 */
function blogtitle_blog_feed_user($account)
{
	$result = db_query_range(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n  WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1 ORDER BY n.created DESC"), $account->uid, 0, variable_get('feed_default_items', 10));
	$channel['title'] = blogtitle_get_blog_title($account->uid, t("@user's blog", array('@user' => $account->name)));
	$channel['link'] = url('blog/'. $account->uid, array('absolute' => TRUE));
	
	$items = array();
	while ($row = db_fetch_object($result))
	{
		$items[] = $row->nid;
	}
	node_feed($items, $channel);
}
